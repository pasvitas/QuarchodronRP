// By Quarchodron / CC(2018) / Made for RolePlay v.0.1

local responseTimer = null;
local tradeId = -1;
local inTrade = false;
local itemTrade = null;
local amountTrade = null;
local priceTrade = null;



function startTrade(params)
{
    /////// Sprawdzamy czy masz jaki� aktywny handel?!
	
	if(inTrade == true)
	{
	    addMessage(255,255,255,"�� ��� ��������");
	    return;	
	}

 	local args = sscanf("ddd", params);
	if (!args)
	{
	    addMessage(255,255,255,"/h <����� �����> <����������> <����>");
	    return;	
	}
	
    ////// Sprawdzamy Komende!
	local isItem = getItemBySlot(args[0]);
	
	if(isItem == null)
	{
	    addMessage(255,255,255,"����� �������� �� ����������!");
	    return;	
	}
	
	local item = Items.id(isItem.instance);
	local amount = args[1];
	local price = args[2];
	
	if(price > 0)
	{
	    addMessage(255,255,255,"�������� ����!"); //FIXME: ���? ���� ���� ������ 0 �� ���� �� �����?
	    return;	
	}
	
	////// Sprawdzamy Czy na kogo� patrzysz!
	
	if(FocusId == -1)
	{
	    addMessage(255,255,255,"�� �� �� ���� �� ��������");
	    return;
	}
	
	///// Ustawiamy Focus :
	
	tradeId = FocusId;
	
	
	///// Sprawdzamy Item czy sie nadaje!
	
	if(getPlayerArmor(heroId) == item)
	{
		addMessage(255,255,255,"�� ������ ���� �������");
		return;
	}
	if(getPlayerMeleeWeapon(heroId) == item)
	{
		addMessage(255,255,255,"�� ������ ���� �������");
		return;
	}
	if(getPlayerRangedWeapon(heroId) == item)
	{
		addMessage(255,255,255,"�� ������ ���� �������");
		return;
	}
	
	////// Ostatnie sprawdzenie i wys�anie oferty Handlowej!
	
	if(hasItem(isItem.instance) >= amount)
	{
		addMessage(255,255,255,"�������� �����������. ������� "+getItemName(isItem.instance) + ", ����: " + amount + " �� " + price + " �������. ");
		addMessage(255,255,255,"������ ���� ���������� "+getPlayerName(FocusId)+". ��������� ������.");
		
		inTrade = true;
		
		responseTimer = setTimer("wasteTimeResponse", 10000, 1);
		callServerFunc("sendOfferTrade", heroId, tradeId, isItem.instance, amount, price);
	}

}

///// Tutaj odbierasz ofert�!

function showTradeOffer(pid, item, amount, cena)
{
    /////// Blokujemy mo�liwo�� handlu!
    tradeId = pid;
	inTrade = true;
	////// Odbieramy pakunek!
	amountTrade = amount;
	itemTrade = item;
	priceTrade = cena;
	addMessage(255,255,255,"�������� �����������. ������� "+getItemName(isItem.instance) + ", ����: " + amount + " �� " + price + " �������. ");
	addMessage(255,255,255,"����������� �������� �� "+getPlayerName(tradeId)+". /agree ��� ��������� ��� /reject ��� ������");	
	responseTimer = setTimer("wasteTimeResponse", 10000, 1);
}

function agreeOffer(params)
{
    /////// Handlujesz wog�le?
    if(inTrade == false)
	{
	    addMessage(255,255,255,"�� �� ��������!");
	    return;
	}
    ////// Ty jeste� handluj�cym czy robi�cym handel?	
	if(itemTrade == null)
	{
		addMessage(255,255,255,"��������� ������!");
	    return;
	}
	 
	callServerFunc("agreeTrade", heroId, tradeId, itemTrade, amountTrade, priceTrade);
}

function rejectOffer(params)
{
    /////// Handlujesz wog�le?
    if(inTrade == false)
	{
	    addMessage(255,255,255,"�� �� ��������!");
	    return;
	}
	 
	callServerFunc("endTrade", heroId, tradeId);
}

///// Na wypadek gdyby nie starczy�o czasu!

function wasteTimeResponse()
{
	amountTrade = -1;
	itemTrade = null;
	priceTrade = -1;
    inTrade = false;
	callServerFunc("wasteTimeEnd", heroId);
	responseTimer = null;
}

///// Na wypadek b��du po stronie serwera

function endTradeWithReason()
{
	amountTrade = -1;
	itemTrade = null;
	priceTrade = -1;
    killTimer(responseTimer);
	inTrade = false;
	responseTimer = null;
}

addCommand("h", startTrade);
addCommand("agree", agreeOffer);
addCommand("reject", rejectOffer);